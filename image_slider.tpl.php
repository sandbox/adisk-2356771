<div id="<?= $carousel_id ?>" class="carousel slide center-block" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach($elements as $delta=>$element): ?>
            <?php $active = $delta == 0 ? 'active' : ''; ?>
            <li data-target="#<?= $carousel_id ?>" data-slide-to="<?= $delta ?>" class="<?= $active ?>"></li>
        <?php endforeach; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach($elements as $delta=>$element): ?>
        <?php
            $active = $delta == 0 ? 'active' : '';
            $src = image_style_url($style, $element['uri']);
        ?>
            <div class="item <?= $active ?>">
                <img src="<?= $src ?>" alt="">
                <div class="carousel-caption"></div>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#<?= $carousel_id ?>" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#<?= $carousel_id ?>" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
